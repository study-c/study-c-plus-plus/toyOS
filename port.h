/********************************************
 * i386 has diffent I/O system for Port (Port I/O)
 * use asm in / out to read and write
 *
 ********************************************/
#ifndef _PORT_H_
#define _PORT_H_
#include "types.h"

class Port {
    protected:
        Port(uint16_t portnumber);
        ~Port();
        uint16_t portnumber;
};

class PortByte: public Port {
    public:
        PortByte(uint16_t portnumber);
        ~PortByte();

        virtual uint8_t Read();
        virtual void Write(uint8_t data);
    
    protected:
        static inline uint8_t ReadByte(uint16_t portnumber) {
            uint8_t result;
            asm volatile("inb %1, %0" : "=a" (result) : "Nd" (portnumber));
            return result;
        }

        static inline void WriteByte(uint16_t portnumber, uint8_t data) {
            asm volatile("outb %0, %1" :: "a" (data), "Nd" (portnumber));
        }
};

class PortByteSlow: public PortByte {
    public:
        PortByteSlow(uint16_t portnumber);
        ~PortByteSlow();

        virtual void Write(uint8_t data);
    
    protected:
        static inline void WriteByteSlow(uint16_t portnumber, uint8_t data) {
            asm volatile("outb %0, %1\njmp 1f\n1: jmp 1f\n1:" :: "a" (data), "Nd" (portnumber));
        }
};

class PortWord: public Port {
    public:
        PortWord(uint16_t portnumber);
        ~PortWord();

        virtual uint16_t Read();
        virtual void Write(uint16_t data);
    
    protected:
        static inline uint16_t ReadWord(uint16_t portnumber) {
            uint16_t result;
            asm volatile("inw %1, %0" : "=a" (result) : "Nd" (portnumber));
            return result;
        }

        static inline void WriteWord(uint16_t portnumber, uint16_t data) {
            asm volatile("outb %0, %1" :: "a" (data), "Nd" (portnumber));
        }
};


class PortLong: public Port {
    public:
        PortLong(uint16_t portnumber);
        ~PortLong();

        virtual uint32_t Read();
        virtual void Write(uint32_t data);
    
    protected:
        static inline uint32_t ReadLong(uint16_t portnumber) {
            uint32_t result;
            asm volatile("inl %1, %0" : "=a" (result) : "Nd" (portnumber));
            return result;
        }

        static inline void WriteLong(uint16_t portnumber, uint32_t data) {
            asm volatile("outb %0, %1" :: "a" (data), "Nd" (portnumber));
        }
};



#endif
