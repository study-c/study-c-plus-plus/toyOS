# set compile variable 
.set MAGIC, 0x1badb002
.set FLAGS, (1<<0 | 1<<1)
.set CHECKSUM, -(MAGIC + FLAGS)

# self defined multiboot for grub
.section .multiboot
    .long MAGIC
    .long FLAGS
    .long CHECKSUM
# programs .text
.section .text
.extern toyMain
.extern callContructors
.global loader # main entry

loader:
    mov $kernal_stack, %esp # save stack top
    call callContructors #init each constructure
    push %eax # multiboot structure
    push %ebx # magic number
    call toyMain
    
_stop:
    cli
    hlt
    jmp _stop

.section .bss
# 4 M stack 
.space 4*1024*1024
kernal_stack:
