#ifndef _GDT_H_
#define _GDT_H_
#include "types.h"

    class GlobalDescriptorTable {

        public:
            // segment descriptors
            class SegmentDescriptor {

                private:
                    uint16_t limit_lo;
                    uint16_t base_lo;
                    uint8_t base_hi;
                    uint8_t type;
                    uint8_t flags_limit_hi;
                    uint8_t base_vhi;
                
                public: 
                    SegmentDescriptor(uint32_t base, uint32_t limit, uint8_t type);
                    uint32_t Base();
                    uint32_t Limit();

            } __attribute__((packed));

        private:
            SegmentDescriptor nullSegmentSelector;
            SegmentDescriptor unusedSegmentSelector;
            SegmentDescriptor codeSegmentSelector;
            SegmentDescriptor dataSegmentSelector;

        public:

            GlobalDescriptorTable();
            ~GlobalDescriptorTable();

            uint16_t CodeSegmentSelector();
            uint16_t DataSegmentSelector();

        protected:
            struct GreatDescriptorTablePointer
                {
                    uint16_t size;
                    uint32_t base;
                } __attribute__((packed)) gdt_pointer;
    };

#endif
