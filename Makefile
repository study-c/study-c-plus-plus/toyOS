# g++ should not using system default libs 
GPPFLAGS = -m32 -fno-use-cxa-atexit -nostdlib -fno-builtin -fno-rtti -fno-exceptions -fno-leading-underscore
ASFLAGS = --32
LDFLAGS = -melf_i386
OBJS = loader.o gdt.o port.o interruptstubs.o interrupts.o keyboard.o kernel.o

%.o: %.cpp
	g++ $(GPPFLAGS) -o $@ -c $<

%.o: %.s
	as $(ASFLAGS) -o $@ $<

toyKernel.bin: linker.ld $(OBJS)
	ld $(LDFLAGS) -T $< -o $@ $(OBJS)

# clean
.PHONY: clean
clean:
	rm toyKernel.bin *.o *.iso
# boot iso disk with grub configure 
toyos.iso: toyKernel.bin
	mkdir iso
	mkdir iso/boot
	mkdir iso/boot/grub
	cp $< iso/boot/
	echo 'set timeout=0' > iso/boot/grub/grub.cfg
	echo 'set default=0' >> iso/boot/grub/grub.cfg
	echo '' >> iso/boot/grub/grub.cfg
	echo 'menuentry "Toy OS" {' >> iso/boot/grub/grub.cfg
	echo ' multiboot /boot/toyKernel.bin' >> iso/boot/grub/grub.cfg
	echo ' boot' >> iso/boot/grub/grub.cfg
	echo '}' >> iso/boot/grub/grub.cfg
	grub-mkrescue --output=$@ iso
	rm -rf iso
# boot iso in qemu
run: toyos.iso
	qemu-system-i386 $< -s

