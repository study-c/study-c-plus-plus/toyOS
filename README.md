### ToyOS

Toy Project is for reviewing my knowledge of OS and C++ / Assemble and fulfilling one of three desired projects

### Description

ToyOS Project is written with C++ and GAS, I started this project while I wanted to restart to learn the modern C++. This will be a long lasting project which I would only focus on the basic knowledge of operation system and networking part. [Check the development journal](https://jerry153fish.github.io/all.html?tag=os) for details

### Milestone

* [Boot problem](https://jerry153fish.github.io/2017/11/08/ToyOS-development-journal-1.html)
* [Memory Segment](https://jerry153fish.github.io/2017/11/09/ToyOS-development-journal-2.html)
* [Interrupts](https://jerry153fish.github.io/2017/12/11/ToyOS-development-journal-3.html)
* Drivers - in process

### Prerequisites

- [Install Ubuntu](https://jerry153fish.github.io/2017/11/20/ubuntu-envirnment-setup.html)
- Install building tools, emulator and other necessary packages

```sh
sudo apt update && sudo apt upgrade
sudo apt install build-essential # building tools including g++ make and bintils which we are going to use
sudo apt install qemu # emulator inside virtualbox 
sudo apt install xorriso # install newest version for iso file generating
# EFT boot system not using virtualbox or other virtual machine
sudo apt install grub-pc-bin # if system use EFI to boot 

```

### Install

```sh
git clone git@gitlab.com:study-c/study-c-plus-plus/toyOS.git
```

### Commands

* build iso file


```sh
make toyos.iso #
```

* run in qemu

```sh
make run
```

* debug in gdb

```sh
gdb

(gdb) target remote localhost:1234 
```

### Acknowledge

Mostly follow [Viktor Engelmann's writting your own operation system tutorial series](https://www.youtube.com/watch?v=AgeX-U4dKSs&index=6&list=PLHh55M_Kq4OApWScZyPl5HhgsTJS9MZ6M) tutorials

[Osdev forum](osdev.org)

### LICENSE

MIT







