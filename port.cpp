#include "port.h"

Port :: Port (uint16_t portnumber) {
    this->portnumber = portnumber;
}

Port::~Port () {}


PortByte :: PortByte (uint16_t portnumber) 
    : Port(portnumber) {}

PortByte :: ~PortByte () {}

void PortByte :: Write (uint8_t data) {
    WriteByte(portnumber, data);
}

uint8_t PortByte :: Read () {
    return ReadByte(portnumber);
}

PortByteSlow :: PortByteSlow (uint16_t portnumber)
    : PortByte(portnumber) {

}

PortByteSlow :: ~PortByteSlow () {}

void PortByteSlow :: Write(uint8_t data) {
    WriteByteSlow(portnumber, data);
}

PortWord :: PortWord (uint16_t portnumber) 
    : Port(portnumber) {
}

PortWord :: ~PortWord () {}

void PortWord :: Write (uint16_t data) {
    WriteWord(portnumber, data);
}

uint16_t PortWord :: Read () {
    return ReadWord(portnumber);
}


PortLong :: PortLong (uint16_t portnumber) 
    : Port(portnumber) {
}

PortLong :: ~PortLong () {}

void PortLong :: Write (uint32_t data) {
    WriteLong(portnumber, data);
}

uint32_t PortLong :: Read () {
    return ReadLong(portnumber);
}


